// Import library ต้องมีตัวแปรมา จะตั้งชื่อตัวแปรเหมือน libery
const express = require('express'); 

//Create express app เรียก ฟังชัน
const app = express()

let todos =[
    {
        student:'Satawat',
        id: 1,
    },
    {
        student:'Benz',
        id: 2,
    }
]

app.get('/todos',(req,res) =>{
    res.send(todos)
})

app.post('/todos',(req,res) =>{
    let newTodo = {
        name: 'Read a book',
        id: 3
    }
    //todos คือ array ที่อยู่ข้างบน newTodo ตัวแปร
    todos.push(newTodo)
    //return HTTP Status:201
    res.status(201).send()
})

app.listen(3000, () =>{
    console.log('TODO API Started PORT 3000')
})
